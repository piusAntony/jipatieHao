<?php
$controller = "HouseTypesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeHouseType');
Route::get('/list',$controller.'listHouseTypes');
