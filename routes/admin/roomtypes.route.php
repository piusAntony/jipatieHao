<?php
$controller = "RoomTypesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeRoomType');
Route::get('/list',$controller.'listRoomTypes');
