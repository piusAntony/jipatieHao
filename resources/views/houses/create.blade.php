@extends('layouts.temp')

@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12 pull-left">

        <!-- Example row of columns -->
        {{--<div class="row col-lg-12 col-md-12 col-sm-12" style="background-color: white; margin: 10px;">--}}
            <form method="post" action="{{ route('houses.store') }}">{{ csrf_field() }}

                <div class="form-group">
                    @if($houseTypes==null)
                        <input type="text" name="house_type_id" value="{{$house_type_id}}" class="form-control">
                </div>
                @endif

                @if($houseTypes !=null)
                    <div class="form-group">
                        <label for="house-content">Select House Type</label>
                        <select name="house_type_id" class="form-control">
                            @foreach($houseTypes as $houseType)
                                <option value="{{$houseType->id}}">{{$houseType->type}}</option>
                            @endforeach
                        </select>
                    </div>
            @endif

        <div class="form-group">
            <label for="house-name">House Name<span class="required">:</span></label>
            <input placeholder="Enter house name" id="house-name" required name="house_name" spellcheck="false"
                   class="form-control"/>
        </div>

        <div class="form-group">
            <label for="house-desc">House Description</label>
            <textarea placeholder="Enter description" style="resize: vertical;" id="house-content"
                      name="house_description" rows="5" spellcheck="false"
                      class="form-control autosize-target text-left"></textarea>
        </div>

        <div class="form-group">
            <label for="house-state">State<span class="required">:</span></label>
            <select required name="state" spellcheck="false" class="form-control">
                <option>Select State</option>
                <option value="Kajiado">Kajiado</option>
                <option value="Nairobi">Nairobi</option>
                <option value="Nakuru">Nakuru</option>
                <option value="Mombasa">Mombasa</option>
                <option value="Kisumu">Kisumu</option>
            </select>
            {{--<input placeholder="County" id="county" required name="state" spellcheck="false" class="form-control"/>--}}
        </div>

        <div class="form-group">
            <label for="house-city">City<span class="required">:</span></label>
            <select required name="city" spellcheck="false" class="form-control">
                <option>Select City</option>
                <option value="Rongai">Rongai</option>
                <option value="Kiserian">Kiserian</option>
                <option value="Kitengela">Kitengela</option>
                <option value="Langata">Langata</option>
                <option value="Karen">Karen</option>
            </select>
            {{--<input placeholder="City" id="city" required name="city" spellcheck="false" class="form-control"/>--}}
        </div>

        <div class="form-group">
            <label for="house-name">Area<span class="required">:</span></label>
            <select required name="area" spellcheck="false" class="form-control" placeholder="Select Area">
                <option>Select Area</option>
                <option value="Nyotu">Nyotu</option>
                <option value="Rongai">Rongai</option>
                <option value="Olekasasi">Olekasasi</option>
                <option value="Maasai lodge">Maasai lodge</option>
                <option value="Tumaini">Tumaini</option>
            </select>

            {{--<input placeholder="Area" id="house-area" required name="area" spellcheck="false" class="form-control"/>--}}
        </div>

        <div class="form-group">
            <label for="house-name">House No Rooms<span class="required">:</span></label>
            <input placeholder="Number Of Rooms" id="house-no_rooms" required name="no_rooms" spellcheck="false"
                   class="form-control"/>
        </div>

        <div class="form-group">
            <input type="hidden" id="lat" required name="latitude" spellcheck="false" class="form-control"/>
            <input type="hidden" id="long" required name="longitude" spellcheck="false" class="form-control"/>
        </div>


        <div class="form-group">
            <div style=" width:700px; height: 500px;" id="map">Asdfg</div>


        </div>
         <div class="form-group">
            <input type="submit" class="btn btn-primary" value="Submit">


        </div>



        </form>

    </div>

    <script type="text/javascript">
        //map.js

        //Set up some of our variables.
        var map; //Will contain map object.
        var marker = false; ////Has the user plotted their location marker?

        //Function called to initialize / create the map.
        //This is called when the page has loaded.
        function initMap() {

            //The center location of our map.
            var centerOfMap = new google.maps.LatLng(-1.396249, 36.7596246);

            //Map options.
            var options = {
                center: centerOfMap, //Set center.
                zoom: 7 //The zoom value.
            };

            //Create the map object.
            map = new google.maps.Map(document.getElementById('map'), options);

            //Listen for any clicks on the map.
            google.maps.event.addListener(map, 'click', function(event) {
                //Get the location that the user clicked.
                var clickedLocation = event.latLng;
                //If the marker hasn't been added.
                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true //make it draggable
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        markerLocation();
                    });
                } else{
                    //Marker has already been added, so just change its location.
                    marker.setPosition(clickedLocation);
                }
                //Get the marker's location.
                markerLocation();
            });
        }

        //This function will get the marker's current location and then add the lat/long
        //values to our textfields so that we can save the location.
        function markerLocation(){
            //Get location.
            var currentLocation = marker.getPosition();
            //Add lat and lng values to a field that we can save.
            document.getElementById('lat').value = currentLocation.lat(); //latitude
            document.getElementById('long').value = currentLocation.lng(); //longitude
        }


        //Load the map when the page has finished loading.
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>


@endsection
