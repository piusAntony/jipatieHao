<div class="modal modal-info" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-labelledby="{{ $modal_id }}_label">
    <div class="modal-dialog animated zoomIn animated-3x modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn btn-danger pull-md-right" style="float: right !important;" data-dismiss="modal" aria-label="Close">&times;</button>
                <h3 class="modal-title" id="{{ $modal_id }}_label">{!! $modal_title !!}</h3>
            </div>
            <div class="modal-body">
                <div class="section">
                    {!! $modal_content !!}
                </div>
            </div>
        </div>
    </div>
</div>