<script type="text/javascript">
    @if(session('force_redirect'))
        window.location.reload(true);
    @endif
    if(typeof jQuery == 'undefined'){
       window.location.reload(true);
    }
</script>
@if(session('message'))
    <div class="alert alert-{{ session('status') }}">{!! session('message') !!}</div>
@endif