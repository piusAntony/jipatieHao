@extends('layouts.app')

@section('title')
    HouseTypes
@endsection

@section('content')
    <a href="#housetype_modal" class="btn btn-primary" data-toggle="modal">ADD HOUSETYPE</a>
    <hr/>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","type","houseType_description","action"],
    'data_url'=>'admin/housetypes/list',
    ])
    @include('common.auto_modal',[
        'modal_id'=>'housetype_modal',
        'modal_title'=>'HOUSETYPE FORM',
        'modal_content'=>Form::autoForm(\App\Models\HouseType::class,"admin/housetypes")
    ])
@endsection