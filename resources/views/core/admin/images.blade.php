@extends('layouts.app')

@section('title')
    Images
@endsection

@section('content')
    <a href="#image_modal" class="btn btn-primary" data-toggle="modal">ADD IMAGE</a>
    <hr/>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","image_name","rooms.roomNo"=>"room_No","action"],
    'data_url'=>'admin/images/list',
    ])
    @include('common.auto_modal',[
        'modal_id'=>'image_modal',
        'modal_title'=>'IMAGE FORM',
        'modal_content'=>Form::autoForm(\App\Models\Image::class,"admin/images")
    ])

    <script type="text/javascript">
        autoFillSelect('room_id','{{ url("admin/rooms/list?all=1") }}');
    </script>
@endsection