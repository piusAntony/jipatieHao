@extends('layouts.app')

@section('title')
    RoomTypes
@endsection

@section('content')
    <a href="#roomtype_modal" class="btn btn-primary" data-toggle="modal">ADD ROOMTYPE</a>
    <hr/>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","roomType","description","action"],
    'data_url'=>'admin/roomtypes/list',
    ])
    @include('common.auto_modal',[
        'modal_id'=>'roomtype_modal',
        'modal_title'=>'ROOMTYPE FORM',
        'modal_content'=>Form::autoForm(\App\Models\RoomType::class,"admin/roomtypes")
    ])
@endsection