@extends('layouts.app')

@section('title')
    Rooms
@endsection

@section('content')
    <a href="#room_modal" class="btn btn-primary" data-toggle="modal">ADD ROOM</a>
    <hr/>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["id","houses.house_name"=>"house_name","roomNo","room_types.roomType"=>"room_type","roomPrice","action"],
    'data_url'=>'admin/rooms/list',
    ])
    @include('common.auto_modal',[
        'modal_id'=>'room_modal',
        'modal_title'=>'ROOM FORM',
        'modal_content'=>Form::autoForm(\App\Models\Room::class,"admin/rooms")
    ])
    <script type="text/javascript">
        autoFillSelect('room_type_id','{{ url("admin/roomtypes/list?all=1") }}');
        autoFillSelect('house_id','{{ url("admin/houses/list?all=1") }}');
    </script>
@endsection