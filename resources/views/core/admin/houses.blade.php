@extends('layouts.app')

@section('title')
    Houses
@endsection

@section('content')
    <a href="#house_modal" class="btn btn-primary" data-toggle="modal">ADD HOUSE</a>
    <hr/>
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["image","id","house_types.type"=>"house_type","user_id","house_name","house_description","state","city","area","latitude","longitude","no_rooms","action"],
    'data_url'=>'admin/houses/list',
    ])
    @include('common.auto_modal',[
        'modal_id'=>'house_modal',
        'modal_title'=>'HOUSE FORM',
        'modal_content'=>Form::autoForm(\App\Models\House::class,"admin/houses")
    ])

    <script type="text/javascript">
        autoFillSelect('house_type_id','{{ url("admin/housetypes/list?all=1") }}');

    </script>
@endsection