<?php

namespace App\Http\Controllers;

use App\Models\House;
use App\Models\HouseType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HousesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $houses=House::all();

        if(Auth::check()){

        $houses=House::where('user_id',Auth::user()->id)->get();
        return view('houses.index',['houses'=>$houses]);
        }
       return view('auth.login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($house_type_id=null)
    {
        //

        $houseTypes=null;

        if(!$house_type_id){
            $houseTypes=HouseType::all();

        }

        return view('houses.create',['house_type_id'=>$house_type_id,'houseTypes'=>$houseTypes]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        if(Auth::check()){
            $house=House::create([
                'house_type_id'=>$request->input('house_type_id'),
                'house_name'=>$request->input('house_name'),
                'house_description'=>$request->input('house_description'),
                'state'=>$request->input('state'),
                'city'=>$request->input('city'),
                'area'=>$request->input('area'),
                'latitude'=>$request->input('latitude'),
                'longitude'=>$request->input('longitude'),
                'no_rooms'=>$request->input('no_rooms'),
                'user_id'=>Auth::user()->id

            ]);

            if($house){
                return redirect()->route('houses.show',['house'=>$house->id])->with('success','house created successifully');
            }

        }

        return back()->with('errors','Error creating new house');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\house  $house
     * @return \Illuminate\Http\Response
     */
    public function show(house $house)
    {
        //
        $house=House::find($house->id);
        return view('houses.show',['house'=>$house]);
        // return view('houses.show',compact('house'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\house  $house
     * @return \Illuminate\Http\Response
     */
    public function edit(house $house)
    {
        //
        $house=House::find($house->id);
        return view('houses.edit',['house'=>$house]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\house  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, house $house)
    {
        //save data
        $houseUpdate=House::where('id',$house->id)->update([

            'house_type_id'=>$request->input('house_type_id'),
            'house_name'=>$request->input('house_name'),
            'house_no_rooms'=>$request->input('house_no_rooms'),
            'house_category'=>$request->input('house_category'),
            'house_description'=>$request->input('house_description'),
            'price'=>$request->input('price'),
            'location_id'=>$request->input('location_id'),

        ]);

        if($houseUpdate){
            return redirect()->route('houses.show',['house'=>$house->id])->with('success','house updated successifuly');
        }

        //redirect
        return back()->withInput();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\house  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(house $house)
    {
        //
        $findhouse=House::find($house->id);
        if($findhouse->delete()){

            //redirect
            return redirect()->route('houses.index')->with('success','house deleted successifully');
        }
        return back()->with('errors','house could not be deleted');

    }
}
