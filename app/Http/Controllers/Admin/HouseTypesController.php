<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\HouseType;
use App\Repositories\SearchRepo;

class HouseTypesController extends Controller
{
        public function index(){
        return view($this->folder.'housetypes',[

        ]);
    }


    public function storeHouseType(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(isset($data['user_id'])){
            if(!$data['user_id'])
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    public function listHouseTypes(){
        $housetypes = HouseType::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $housetypes->select('id','type as name')->get();
        return SearchRepo::of($housetypes)
            ->addColumn('action',function($housetype){
                $str = '';
                $json = json_encode($housetype);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'housetype_modal\');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })->make();
    }
}
