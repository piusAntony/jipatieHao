<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Image;
use App\Models\Room;
use App\Repositories\SearchRepo;

class ImagesController extends Controller
{
        public function index(){
        return view($this->folder.'images',[

        ]);
    }


    public function storeImage(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(isset($data['user_id'])){
            if(!$data['user_id'])
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    public function listImages(){
        $images = Image::join('rooms','rooms.id','=','images.room_id')->where([
            ['images.id','>',0]
        ])->select('images.*','room.roomNo as name');
        if(\request('all'))

            return $images->select('images.id','room_name as name')->get();
        return SearchRepo::of($images)
            ->addColumn('action',function($image){
                $str = '';
                $json = json_encode($image);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'image_modal\');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })->make();
    }
}
