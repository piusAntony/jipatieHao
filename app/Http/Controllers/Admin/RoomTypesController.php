<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\RoomType;
use App\Repositories\SearchRepo;

class RoomTypesController extends Controller
{
        public function index(){
        return view($this->folder.'roomtypes',[

        ]);
    }


    public function storeRoomType(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(isset($data['user_id'])){
            if(!$data['user_id'])
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    public function listRoomTypes(){
        $roomtypes = RoomType::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            //return $roomtypes->get();
            return $roomtypes->select('id','roomtype as name')->get();
        return SearchRepo::of($roomtypes)
            ->addColumn('action',function($roomtype){
                $str = '';
                $json = json_encode($roomtype);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'roomtype_modal\');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })->make();
    }
}
