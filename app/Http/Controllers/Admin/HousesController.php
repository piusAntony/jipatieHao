<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\House;
use App\Models\HouseType;
use App\Repositories\SearchRepo;

class HousesController extends Controller
{
        public function index(){
        return view($this->folder.'houses',[

        ]);
    }


    public function storeHouse(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();        
        $data['user_id'] = request()->user()->id;
        if(request()->file('image')){
            $file = request()->file('image');
            $file_name = $file->getClientOriginalName();
//            $name_arr = explode('.',$file_name);
//            $ext = $name_arr[count($name_arr)-1];
//            $file_name = str_replace($ext,'',$file_name);
//            $file_name = str_slug($file_name).'.'.$ext;
            $file_name = time().'-'.str_random(5).'-'.$file_name;
            $file->move(storage_path('app/public/house-images'),$file_name);
            $file_path = 'storage/house-images/'.$file_name;
            $data['image'] = $file_path;
        }
        $this->autoSaveModel($data);

        return redirect()->back();
    }

    public function listHouses(){
        $houses = House::join('house_types','house_types.id','=','houses.house_type_id')->where([
            ['houses.id','>',0]
        ])->select('houses.*','house_types.type as house_type');
        if(\request('all'))
            //return $houses->get();
            return $houses->select('houses.id','house_name as name')->get();
        return SearchRepo::of($houses)
            ->addColumn('image',function($house){
                if($house->image)
                    return '<img style="height:150px;" src="'.url($house->image).'">';
                return 'N/A';
            })
            ->addColumn('action',function($house){
                $str = '';
                $json = json_encode($house);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'house_modal\');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })->make();
    }
}
