<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Room;
use App\Models\House;
use App\Repositories\SearchRepo;

class RoomsController extends Controller
{
        public function index(){
        return view($this->folder.'rooms',[

        ]);
    }


    public function storeRoom(){
        //{autoFillSelect('room_type_id','{{ url("admin/roomtypes/list?all=1") }}');
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(isset($data['user_id'])){
            if(!$data['user_id'])
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    public function listRooms(){
//        $rooms = Room::where([
//            ['id','>',0]
//        ]);

        $rooms = Room::join('room_types','room_types.id','=','rooms.room_type_id')
            ->join('houses','houses.id','=','rooms.house_id')->where([
            ['rooms.id','>',0]
        ])->select('rooms.*','room_types.roomType as room_type','houses.house_name as house_name','rooms.roomNo as name');

        if(\request('all'))
            return $rooms->get();
        return SearchRepo::of($rooms)
            ->addColumn('action',function($room){
                $str = '';
                $json = json_encode($room);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'room_modal\');" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })->make();
    }
}
