<?php
/**
 * Created by PhpStorm.
 * User: iankibet
 * Date: 7/12/17
 * Time: 4:09 AM
 */

namespace App\Repositories;
use phpDocumentor\Reflection\Types\Self_;
use Route;

class SearchRepo
{
    protected static $data;
    protected static $request_data;
    protected static $instance;
    public static function of($model){
        self::$instance = new self();
        $request_data = request()->all();
        self::$request_data = $request_data;
        if(isset($request_data['filter_key'])){
            $value = @$request_data['filter_value'];
            $key = $request_data['filter_key'];
            if(isset($value))
                $model = $model->where([
                    [$key,'like',"%".$value."%"]
                ]);
        }
        $request_data = self::$request_data;
        if(isset($request_data['order_by']) && isset($request_data['order_method'])){
            $model = $model->orderBy($request_data['order_by'],$request_data['order_method']);
        }else{
            $model = $model->orderBy('created_at','desc');

        }
        if(isset($request_data['per_page'])){
            $data =  $model->paginate(round($request_data['per_page'],0));
        }else{
            $data= $model->paginate(10);
        }
        self::$data = $data;
       return self::$instance;
    }

    public static function make($pagination = true){
        $data = self::$data;
        $request_data = self::$request_data;
        unset($request_data['page']);
        $data->appends($request_data);
        if($pagination){
            $pagination = $data->links('vendor.pagination.bootstrap-4')->__toString();
            $data = $data->toArray();
            $data['pagination'] = $pagination;
        }
        return $data;

    }

    public static function addColumn($column,$function){
        $records = self::$data;
        foreach($records as $index=>$record){
            $record->$column = $function($record);
            $records[$index] = $record;
        }
        self::$data = $records;
        return self::$instance;
    }
}