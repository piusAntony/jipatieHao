<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RoomType;
use App\Models\House;
use App\Models\Image;
class Room extends Model
{
    
	protected $fillable = ["house_id","roomNo","room_type_id","roomPrice"];

    public function roomTypes(){
        return $this->belongsTo(RoomType::class);
    }
    public function house(){
        return $this->belongsTo(House::class);
    }
    public function images(){
        return $this->hasMany(Image::class);
    }
}
