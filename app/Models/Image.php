<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Room;
class Image extends Model
{
    
	protected $fillable = ["image_name","room_id"];

    public function room(){
        return $this->belongTo(Room::class);
    }
}
