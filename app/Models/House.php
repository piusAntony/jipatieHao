<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\HouseType;
use App\Models\Room;
class House extends Model
{
    
	protected $fillable = ["house_type_id","house_name","house_description","state","city","area","latitude","longitude","no_rooms",'image'];

     public function houseTypes(){
        return $this->belongsTo(HouseType::class);
    }

    public function rooms(){
        return $this->hasMany(Room::class);
    }
}
