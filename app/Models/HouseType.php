<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\House;
class HouseType extends Model
{
    
	protected $fillable = ["type","houseType_description"];

	public function houses(){
        return $this->hasMany(House::class);
    }

}
